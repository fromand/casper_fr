# Casper_FR

Version française du thème par défault de
[Ghost](http://github.com/tryghost/ghost/) disponible ici [releases](https://github.com/TryGhost/Casper/releases).


## Installation du thème

### Préparation de l'archive du thème

    > git clone https://fromand@bitbucket.org/fromand/casper_fr.git
    > cd casper_fr
    > rm -Rf **/.git
    > rm -Rf .git*
    > cd ..
    > zip -r casper_fr.zip casper_fr

### Chargement de l'archive

Dans Ghost, dans la section *Settings*, *General*, *Theme*,
téléchargez l'archive *casper_fr.zip* du thème puis activez le thème *Casper_FR*.

## Afficher les dates au format français

Ajouter à la fin du fichier */core/server/helpers/date.js*

    > echo "moment.lang('fr')" >>/core/server/helpers/date.js


## Crédit de traduction

- Fabrice Romand <fabrice.romand AT gmail DOT com>
